# encoding: UTF-8
require 'uri'
require 'net/http'
require 'open-uri'
class HomeController < ApplicationController
  FILMWEB_URL = 'http://www.filmweb.pl'

  def index
    @movies = movies
  end

  private

  def cinema_city_url(date)
    "https://www.cinema-city.pl/pl/data-api-service/v1/quickbook/10103/film-events/in-cinema/1070/at-date/#{date.to_s}?attr=&lang=pl_PL"
  end

  def filmweb_url(search_text)
    "http://www.filmweb.pl/search/film?q=#{search_text}&startYear=&endYear=&startRate=&endRate=&startCount=&endCount=&sort=PREMIERE_POLAND&sortAscending=false"
  end

  def movies
    Rails.cache.fetch("movies/#{Date.today.to_s}") do
      movie_titles = fetch_cinema_city_movies
      movies = movie_titles.map do |movie|
        fetch_filmweb_info(movie)
      end
      movies.sort! { |x, y| y[:rating] <=> x[:rating] }
      movies
    end
  end


  def fetch_cinema_city_movies #todo concurrent fetching?
    fetch_cinema_city_movies_for_day(Date.today).concat(
      fetch_cinema_city_movies_for_day(Date.tomorrow)).uniq
  end

  def fetch_cinema_city_movies_for_day(date)
    response = Net::HTTP.get(URI.parse(cinema_city_url(date)))
    JSON.parse(response)['body']['films'].map { |f| { name: f['name'], video_url: f['videoLink'] } }
  end

  def fetch_filmweb_info(movie)
    search_text = ActiveSupport::Inflector.transliterate(movie[:name].strip).to_s #todo: hack (nokogiri problems with accented chars)
    search_text = CGI.escape(search_text)
    search_page = Nokogiri::HTML(open(filmweb_url(search_text)).read)
    link = search_page.at_css('.resultsList .filmPreview__link')['href']
    details_url = URI.join(FILMWEB_URL, link).to_s
    movie_page = Nokogiri::HTML(open(details_url).read)
    raw_year = movie_page.at_css('.filmTitle span')&.text&.strip
    {
      title: movie_page.at_css('.filmTitle a')&.text&.strip || '-',
      year: raw_year&.gsub(/[()]/, '') || '-',
      rating: movie_page.at('[itemprop=ratingValue]')&.text&.strip,
      description: movie_page.at_css('.filmPlot p')&.text&.strip || '-',
      details_url: details_url,
      video_url: movie[:video_url]
    }
  end

end
